package main

import (
	"bytes"
	"fmt"
	"os"
	"path"
	"strings"

	"github.com/alecthomas/template"
	"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/protoc-gen-go/descriptor"
	plugin "github.com/golang/protobuf/protoc-gen-go/plugin"
	"github.com/pkg/errors"
)

type generator struct{}

func (g *generator) Generate(req *plugin.CodeGeneratorRequest) (*plugin.CodeGeneratorResponse, error) {
	resp := &plugin.CodeGeneratorResponse{}

	for _, name := range req.FileToGenerate {
		file, err := getFileDescriptor(req, name)
		if err != nil {
			return nil, err
		}

		// only generating the service, protoc already handles csharp messages
		if len(file.Service) == 0 {
			continue
		}

		genFile, err := g.generateFile(file)
		if err != nil {
			return nil, errors.Wrapf(err, "generating %q", name)
		}

		// Add the generated file to the response
		resp.File = append(resp.File, genFile)
	}

	return resp, nil
}

func (g *generator) generateFile(file *descriptor.FileDescriptorProto) (*plugin.CodeGeneratorResponse_File, error) {
	var err error

	buffer := &bytes.Buffer{}

	tmpl := template.New("csharp_file")
	tmpl, err = tmpl.Parse(CSharpTemplate)
	if err != nil {
		wd, _ := os.Getwd()
		return nil, errors.Wrap(err, "parsing template "+wd)
	}

	// TODO: modulize
	// TODO: camelize
	presenter := &csharpTemplatePresenter{
		proto: file,

		Version:        "1.0.0",
		SourceFilename: file.GetName(),
		Package:        file.GetPackage(),
	}

	err = tmpl.Execute(buffer, presenter)
	if err != nil {
		return nil, errors.Wrapf(err, "rendering template for %q", file.GetName())
	}

	resp := &plugin.CodeGeneratorResponse_File{}
	resp.Name = proto.String(getOutputFilename(file))
	resp.Content = proto.String(buffer.String())

	return resp, nil
}

// getFileDescriptor finds the FileDescriptorProto for the given filename.
// Returns a error if the descriptor could not be found.
func getFileDescriptor(req *plugin.CodeGeneratorRequest, name string) (*descriptor.FileDescriptorProto, error) {
	for _, descriptor := range req.ProtoFile {
		if descriptor.GetName() == name {
			return descriptor, nil
		}
	}

	return nil, fmt.Errorf("could not find descriptor for %q", name)
}

// getOutputFilename determines what the filename should be for the generated
// csharp code.
func getOutputFilename(file *descriptor.FileDescriptorProto) string {
	name := file.GetName()
	name = strings.TrimSuffix(name, path.Ext(name))

	return name + ".twirp.cs"
}
